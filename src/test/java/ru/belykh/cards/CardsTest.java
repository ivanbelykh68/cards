package ru.belykh.cards;

import org.junit.Test;
import static org.junit.Assert.*;

import java.io.File;

/**
 * Unit test for CardsTest.
 */
public class CardsTest {

	@Test
	public void testCards() {
		File dir = new File(ClassLoader.getSystemResource("imgs").getFile());
		for(File image : dir.listFiles()){
			String name = image.getName().substring(0, image.getName().indexOf('.'));
			String result = Cards.getImageCards(image.getPath());
			assertEquals(name, result);
		}
	}


}
