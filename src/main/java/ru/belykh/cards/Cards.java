package ru.belykh.cards;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Cards {
	//Черно-белые матрицы шаблонов масти и значений. 0 - фон (белый), 1 - отличный от фона цвет
	private static Map<String, int[][]> suitMap;
	private static Map<String, int[][]> valueMap;
	//Получить черно-белую матрицу части изображения. Необходимо передать координаты точки белого цвета.
	private static int[][] loadImage(BufferedImage bufferedImage, int whiteX, int whiteY, int offsetX, int offsetY, int width, int height) {
		if (width == 0) width = bufferedImage.getWidth();
		if (height == 0) height = bufferedImage.getHeight();
		int whiteRGBValue = bufferedImage.getRGB(whiteX, whiteY);
		int[][] result = new int[width][height];
		for (int x = offsetX; x < width + offsetX; x++) {
			for (int y = offsetY; y < height + offsetY; y++) { result[x - offsetX][y - offsetY] = bufferedImage.getRGB(x, y) == whiteRGBValue ? 0 : 1; }
		}
		return result;
	}
	//Получить черно-белую матрицу части изображения. Необходимо передать координаты точки белого цвета.
	private static int[][] loadImage(InputStream inputStream, int whiteX, int whiteY, int offsetX, int offsetY, int width, int height) {
		try {
			BufferedImage image = ImageIO.read(inputStream);
			int[][] result = loadImage(image, whiteX, whiteY,  offsetX, offsetY, width, height);
			inputStream.close();
			return result;
		} catch (IOException e) { e.printStackTrace(); }
		return new int[0][0];
	}
	//Получить минимальное количество различающихся точек между исходныи изображеним и шаблоном в заданной области. Меньше результат - больше соответствия.
	private static int getMinDifference(int[][] image, int[][] template, int startX, int startY, int endX, int endY) {
		if (template.length == 0 || template[0].length == 0) return image.length * image[0].length;
		int minDifference = template.length * template[0].length;
		for (int imageX = startX; imageX <= image.length - template.length && imageX < endX; imageX++) {
			for (int imageY = startY; imageY <= image[0].length - template[0].length && imageX < endY; imageY++) {
				int dif = 0;
				for (int templateX = 0; templateX < template.length; templateX++) {
					for (int templateY = 0; templateY < template[0].length; templateY++) {
						dif += Math.abs(image[imageX + templateX][imageY + templateY] - template[templateX][templateY]);
					}
				}
				if (dif < minDifference) minDifference = dif;
			}
		}
		return minDifference;
	}
	//Получить код наилучшего результата соответствия картинки с матрицой. Возвращает строку с мастью или значением, в зависимости от тогоЮ что передано в качестве шаблона
	private static String getBestResult(int[][] image, Map<String, int[][]> templates, int startX, int startY, int endX, int endY) {
		int minDif = image.length * image[0].length;
		String bestResultKey = null;
		for(Map.Entry<String, int[][]> entry : templates.entrySet()){
			int result = getMinDifference(image, entry.getValue(), startX, startY, endX, endY);
			if(result < minDif){
				minDif = result;
				bestResultKey = entry.getKey();
			}
		}
		return bestResultKey;
	}
	//Формирует строку найденных карт по имени файла изображения. Может вернуть null, если ничего не найдено
	public static String getImageCards(String filename) {
		try {
			BufferedImage bufferedImage = ImageIO.read(new FileInputStream(filename));
			if(suitMap == null) {
				suitMap = Arrays.asList("c", "d", "h", "s").stream().collect(Collectors.toMap(s -> s, s -> loadImage(ClassLoader.getSystemResourceAsStream("suit/" + s + ".png"), 0, 0, 0, 0, 0, 0)));
				valueMap = Arrays.asList("2", "3", "4", "5", "6", "7", "8", "9", "10", "A", "Q", "K", "J").stream().collect(Collectors.toMap(s -> s, s -> loadImage(ClassLoader.getSystemResourceAsStream("value/" + s + ".png"), 28, 23, 0, 0, 0, 0)));
			}
			StringBuffer result = new StringBuffer();
			List<int[][]> potencialCards = Arrays.asList(147, 219, 291, 363, 435, 507).stream().map(x -> loadImage(bufferedImage, x + 27, 631, x, 591, 54, 79)).collect(Collectors.toList());
			for(int[][] card : potencialCards){
				if(getMinDifference(card, new int[20][20], 0, 0, card.length, card[0].length) != 0) continue; //white area test
				result.append(getBestResult(card, valueMap, 0, 0, 16, 16));
				result.append(getBestResult(card, suitMap, 17, 40, 33, 58));
			}
			if(result.length() > 0) return result.toString();
		} catch (IOException e) { e.printStackTrace(); }
		return null;
	}
	//Сканирует директорию по пути, переданному первый параметром. Обходит все вложенные изображения и печатает отчет по изображениям, на которых найдены карты.
	public static void main(String[] args) {
		if(args.length < 1) return;
		File folder = new File(args[0]);
		for (File imageFile : folder.listFiles()){
			String cards = getImageCards(imageFile.getPath());
			if(cards != null) System.out.println(imageFile.getName() + " - " + cards);
		}
	}
}